import { Component, Input, Output, EventEmitter } from "@angular/core";
import * as SurveyKo from "survey-knockout";
import * as SurveyCreator from "survey-creator";
import * as widgets from "surveyjs-widgets";

import "inputmask/dist/inputmask/phone-codes/phone.js";
import { ruStrings } from 'src/assets/russian';
import { DataService } from '../services/data.service';

widgets.icheck(SurveyKo);
widgets.select2(SurveyKo);
widgets.inputmask(SurveyKo);
widgets.jquerybarrating(SurveyKo);
widgets.jqueryuidatepicker(SurveyKo);
widgets.nouislider(SurveyKo);
widgets.select2tagbox(SurveyKo);
widgets.signaturepad(SurveyKo);
widgets.sortablejs(SurveyKo);
widgets.ckeditor(SurveyKo);
widgets.autocomplete(SurveyKo);
widgets.bootstrapslider(SurveyKo);
//widgets.emotionsratings(SurveyKo);

SurveyCreator.StylesManager.applyTheme("default");

var CkEditor_ModalEditor = {
  afterRender: function(modalEditor, htmlElement) {
    var editor = window["CKEDITOR"].replace(htmlElement);
    editor.on("change", function() {
      modalEditor.editingValue = editor.getData();
    });
    editor.setData(modalEditor.editingValue);
  },
  destroy: function(modalEditor, htmlElement) {
    var instance = window["CKEDITOR"].instances[htmlElement.id];
    if (instance) {
      instance.removeAllListeners();
      window["CKEDITOR"].remove(instance);
    }
  }
};
SurveyCreator.SurveyPropertyModalEditor.registerCustomWidget(
  "html",
  CkEditor_ModalEditor
);

@Component({
  selector: 'app-survey-creator',
  templateUrl: './survey.creator.component.html',
  styleUrls: ['./survey.creator.component.css']
})
export class SurveyCreatorComponent {
  surveyCreator: SurveyCreator.SurveyCreator;
  @Input() json: any;
  @Output() surveySaved: EventEmitter<Object> = new EventEmitter();
  
  constructor(private dataService: DataService) {}
  
  ngOnInit() {
    this.json = this.dataService.getSurvey();
    SurveyKo.JsonObject.metaData.addProperty(
      "questionbase",
      "popupdescription:text"
    );
    SurveyKo.JsonObject.metaData.addProperty("page", "popupdescription:text");

    //Create your translation
    var russianStrings = ruStrings;
    //Set the your translation to the locale
    SurveyCreator.editorLocalization.locales["ru"] = russianStrings;
    SurveyCreator.editorLocalization.currentLocale = "ru";
    // SurveyCreator.haveCommercialLicense = true;

    //Limited the number of showing locales in survey.locale property editor
    SurveyKo.surveyLocalization.supportedLocales = ["en", "ru"];
    let options = { 
      showEmbededSurveyTab: true, 
      generateValidJSON: true,
      showTranslationTab: true,
    };

    this.surveyCreator = new SurveyCreator.SurveyCreator(
      "surveyCreatorContainer",
      options
    );
    this.surveyCreator.text = JSON.stringify(this.json);
    this.surveyCreator.saveSurveyFunc = this.saveMySurvey;
    this.surveyCreator.toolbox.changeCategories([
      { name: "panel", category: "Разделы" }, 
      { name: "paneldynamic", category: "Разделы" }, 
      { name: "flowpanel", category: "Разделы" }, 
      { name: "matrix", category: "Сетки" },
      { name: "matrixdropdown", category: "Сетки" },
      { name: "matrixdynamic", category: "Сетки" },

    ]);

  }

  saveMySurvey = () => {
    this.dataService.saveSurvey(JSON.parse(this.surveyCreator.text));
    this.surveySaved.emit(JSON.parse(this.surveyCreator.text));
  };
}