import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './components/admin/admin.component';
import { AnalyticsComponent } from './components/analytics/analytics.component';
import { SurveyComponent } from './survey/survey.component';
import { SurveyCreatorComponent } from './survey.creator/survey.creator.component';
import { SurveyAnalyticsComponent } from './survey.analytics/survey.analytics.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './layout/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AnalyticsComponent,
    SurveyComponent,
    SurveyCreatorComponent,
    SurveyAnalyticsComponent,
    HomeComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
