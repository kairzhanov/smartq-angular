import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient
  ) { }

  getSurvey() {
    if (localStorage.getItem('survey') != null)
      return JSON.parse(localStorage.getItem('survey'));
    
    this.http.get('/assets/json/survey.json').subscribe(result => {
      return result
    }, error => {
      console.log(error);
      return null;
    });
  }

  saveSurvey(json: any) {
    var str = JSON.stringify(json);
    localStorage.setItem('survey', str);
  }
}
